//
//  UserClass.swift
//  iTravel
//
//  Created by Ali Raza Noorani on 2019-11-09.
//  Copyright © 2019 Ali Raza Noorani. All rights reserved.
//

import UIKit

class UserClass: NSObject {
    
    public var userName: String
    public var userAccount:Decimal
    
    
    
    static let share = UserClass(userName: "Chris", userAccount: 100.00)
    
    public init(userName:String,userAccount:Decimal){
        
        self.userName =  userName;
        self.userAccount = userAccount
        
        
    }
    
    public var getUserName:String{
        
    return userName
        
    }
    
    public var getUserPoints:Int{
           
    return Services.shared.getPoints()
           
    }
    
    public var getUserAmount:Decimal{
        
    return userAccount
    }
    
    public func setUserAmount(){
        
        let newAmount =  getUserAmount - 3.50
        self.userAccount = newAmount
    }
    
    
    public var getUserUID:String{
           
    return UIDevice.current.identifierForVendor!.uuidString

    }
    
    

}
