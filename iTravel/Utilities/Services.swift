//
//  Services.swift
//  iTravel
//
//  Created by Ali Raza Noorani on 2019-11-08.
//  Copyright © 2019 Ali Raza Noorani. All rights reserved.
//

import UIKit

import CoreBluetooth
import CoreLocation
class Services {
    
    static let shared = Services();
    struct preferancesKey {
        static let pointsKey: String = "userPoints"
     
     }
    
    
    public func setPoints(points:Int){
    UserDefaults.standard.set(points + getPoints(), forKey: preferancesKey.pointsKey)
    }
    
    
    public func getPoints() -> Int{
        var points:Int = 0;
        if(isKeyPresentInUserDefaults(key:preferancesKey.pointsKey)){
            points = UserDefaults.standard.integer(forKey: preferancesKey.pointsKey)
        }
        
        return points
    }
    
   public func startScanning() {
        let uuid = UUID(uuidString: "13625307-6DD6-45FE-BEBF-B879287E8C46")!
        let beaconRegion = CLBeaconIdentityConstraint(uuid:uuid, major: 100, minor: 2)
        LocationClass.shareedLocation.locationManager.startRangingBeacons(satisfying: beaconRegion)
  
    let beaconReg = CLBeaconRegion(uuid: uuid, major: 100, minor: 2, identifier: "com.itravel.beacon")
  
    LocationClass.shareedLocation.locationManager.startMonitoring(for: beaconReg)
    }
    
    
    
    
    
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    

}
