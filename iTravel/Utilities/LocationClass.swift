//
//  LocationClass.swift
//  iTravel
//
//  Created by Ali Raza Noorani on 2019-11-09.
//  Copyright © 2019 Ali Raza Noorani. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications

class LocationClass: NSObject,CLLocationManagerDelegate {
 var locationManager: CLLocationManager!
    var count:Int = 0
    var locationString = [String]()
   
  
    var startLat:Double?
    var startLon:Double?
    
    struct locationEndData {
        var endLat: Double?
        var endLon: Double?
    }
    
    static let shareedLocation = LocationClass()
    
    public override init(){
        super.init()
        locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.distanceFilter = 5
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
       
       // locationManager.startUpdatingLocation()
        //locationManager.startMonitoringSignificantLocationChanges()
        }
    
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            // you're good to go!
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
           print("New location is \(location)")
            
            Services.shared.startScanning()
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didRange beacons: [CLBeacon], satisfying beaconConstraint: CLBeaconIdentityConstraint) {
     if beacons.count > 0 {
                 //  updateDistance(beacons[0].proximity)
    } else {
                   //updateDistance(.unknown)
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        
    }
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
          switch state {
          case .inside:
           beconEntered()
          case .outside:
                 print("Checked Out!")
            beconExited()
          case .unknown:
                  print("Unkown")
              default:
                print("BeaconManager:didDetermineSt")
    }
    
    }
   
    
      private func beconExited(){
        let coordinate₀ = CLLocation(latitude:locationManager.location?.coordinate.latitude ?? 0.0, longitude: locationManager.location?.coordinate.longitude ?? 0.0)
                  let coordinate₁ = CLLocation(latitude:self.startLat ?? 0.0, longitude: self.startLon ?? 0.0)
        
                  let distanceInMeters =  coordinate₁.distance(from: coordinate₀)
                  let points = distanceInMeters < 1 ? 1 : distanceInMeters * 2
        
        
         print("distance \(distanceInMeters)")
        print("Points \(points)")
        Services.shared.setPoints(points: Int(points))
        
        NotificationCenter.default
        .post(name:           NSNotification.Name("updateText"),
         object: nil)
        
        
        
        
        
        
        
        }
    
    
    
    private func beconEntered(){
        self.startLat =  locationManager.location?.coordinate.latitude
        self.startLon =  locationManager.location?.coordinate.longitude
         print("\(locationManager.location?.coordinate.latitude) - \(locationManager.location?.coordinate.longitude) ")
    }
    

}

