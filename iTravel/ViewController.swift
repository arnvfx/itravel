//
//  ViewController.swift
//  iTravel
//
//  Created by Ali Raza Noorani on 2019-11-09.
//  Copyright © 2019 Ali Raza Noorani. All rights reserved.
//

import UIKit
import Lottie
class ViewController: UIViewController {
     let animationView = AnimationView()
   
    @IBOutlet weak var animationUIView: UIView!
    @IBOutlet weak var pointsLable: UILabel!
    @IBOutlet weak var myButton:UIButton!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playAnimation()
        let points = String(UserClass.share.getUserPoints)
        let welcomeText:String = "Hey \(UserClass.share.getUserName) your score is  \(points)"
        
        pointsLable.text = welcomeText
        startScanningForBecon()
        
        NotificationCenter.default
         .addObserver(self,
                      selector: #selector(updateText),
        name: NSNotification.Name ("updateText"),                                           object: nil)
       
        // Do any additional setup after loading the view.
    }
    
    @objc func updateText(){
        let points = String(UserClass.share.getUserPoints)
              let welcomeText:String = "Hey \(UserClass.share.getUserName) your score is  \(points)"
              
              pointsLable.text = welcomeText
        
    }
    
    func startScanningForBecon(){
        
        Services.shared.startScanning()
    }
    
    
    
    func playAnimation(){
         let animation = Animation.named("1042-round")
        animationView.frame = CGRect(x: 0, y: 0, width: 300, height: 300)
        animationView.animation = animation
     
         animationView.contentMode = .scaleAspectFit
         
         self.animationUIView.addSubview(animationView)
        
    }
    
    
   
   override func viewDidAppear(_ animated: Bool) {
        
        animationView.play(fromProgress: 0,
                            toProgress: 1,
                            loopMode: LottieLoopMode.autoReverse,
                            completion: { (finished) in
                             if finished {
                               print("Animation Complete")
                             } else {
                               print("Animation cancelled")
                             }
         })
     }

    @IBAction func actionButton(_ sender: Any) {
           Services.shared.startScanning()
    }
    
}

