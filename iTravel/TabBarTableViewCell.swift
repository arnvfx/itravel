//
//  TabBarTableViewCell.swift
//  iTravel
//
//  Created by Shoko Hashimoto on 2019-11-09.
//  Copyright © 2019 Ali Raza Noorani. All rights reserved.
//

import UIKit

class TabBarTableViewCell: UITableViewCell {

    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var points: UILabel!
}
