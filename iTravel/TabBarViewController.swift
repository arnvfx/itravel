//
//  TabBarViewController.swift
//  iTravel
//
//  Created by Shoko Hashimoto on 2019-11-09.
//  Copyright © 2019 Ali Raza Noorani. All rights reserved.
//

import UIKit

class TabBarViewController: UIViewController {
    
    let tableData = [["Free daily pass","1"],
    ["Skytrain daily pass","1"],
    ["Amazon coupon","2"],
    ["Ticket to Brazil","3"],
    ["Monthly free music","4"]]
    
    var currentPoint = Int(UserClass.share.getUserPoints)
    
    @IBOutlet weak var couponTable: UITableView!
    @IBOutlet weak var pointLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        
            
        
        
        pointLabel.text = "\(currentPoint) Points"
    }
   
    override func viewDidAppear(_ animated: Bool) {
        var currentPoint = Int(UserClass.share.getUserPoints)
        pointLabel.text = "\(currentPoint) Points"
    }
    
}


    


extension TabBarViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = couponTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TabBarTableViewCell
        let data = tableData[indexPath.row]
        cell.title.text = data[0]
        cell.points.text = data[1]
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = tableData[indexPath.row]
        var massage = ""
        if let points = Int(data[1]) {
            if currentPoint >= points {
                massage = "You got \(data[0]) coupon!"
                currentPoint = currentPoint - points
            } else {
                let missingPoints = points - currentPoint
                massage = "You still need to get \(missingPoints) points!"
            }
        
        
            let alertController = UIAlertController(title: massage, message: nil, preferredStyle: .alert)
            
            let action = UIAlertAction(title: "OK", style: .cancel) { (action) in
                _ = self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(action)
            present(alertController, animated: true, completion: nil)
            
            pointLabel.text = "\(currentPoint) Points"
            Services.shared.setPoints(points:currentPoint)
        }
    }

}
